-- DROP DATABASE IF EXISTS tutorial;
-- CREATE DATABASE tutorial

DROP EXTENSION IF EXISTS tutorial_fdw CASCADE;
CREATE EXTENSION tutorial_fdw;
CREATE SERVER tutorial_server FOREIGN DATA WRAPPER tutorial_fdw;
CREATE FOREIGN TABLE sequential_ints ( val int ) SERVER tutorial_server OPTIONS ( start '10', end '24' );
SELECT * FROM sequential_ints;

DROP EXTENSION IF EXISTS postgres_fdw CASCADE;
CREATE EXTENSION postgres_fdw;
CREATE SERVER obm FOREIGN DATA WRAPPER postgres_fdw;
CREATE FOREIGN TABLE sequential_ints ( val int ) SERVER tutorial_server OPTIONS ( start '10', end '24' );
SELECT * FROM sequential_ints;

