A tutorial for postgresql fdw by https://www.dolthub.com/blog/2022-01-26-creating-a-postgres-foreign-data-wrapper/

To try it out install lando from https://docs.lando.dev/basics/installation.html

```sh
lando start
lando make
lando make_install
lando smoke_test
```
